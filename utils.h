/* Dawid Lazarczyk, 337614, SO zad. nr 2 */
#ifndef _UTILS_
#define _UTILS_

#include "err.h"

#define MAX_DATA_SIZE 5000

typedef struct {
    long mtype;
    char mtext[MAX_DATA_SIZE];
} Msg;

#define DKEY 4410
#define RSKEY 5595
#define RLKEY 1129

#endif
