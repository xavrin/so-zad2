#ifndef _ERR_
#define _ERR_

/* print system call error message and terminate */
extern void syserr(const char *fmt, ...);

/* print system call error message and terminate
 * version for the pthread functions calls */
extern void psyserr(int bl, const char *fmt, ...);

/* print error message and terminate */
extern void fatal(const char *fmt, ...);

#endif
