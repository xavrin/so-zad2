CC = cc

CFLAGS = -Wall
LDFLAGS = -Wall -pthread
ALL = serwer klient

all: $(ALL)

%.o: %.c
	$(CC) $(FLAGS) -c $<

$(ALL): %: %.o utils.h err.o
	$(CC) $(LDFLAGS) -o $@ $^
.PHONY: clean

clean:
	rm -fr *.o
