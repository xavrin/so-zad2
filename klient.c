/* Dawid Lazarczyk, 337614, SO zad. nr 2 */
#include <sys/types.h>
#include <sys/msg.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include "utils.h"

#define TRY(cond, name) if ((cond)) syserr(name)

#define ARGS_NUM 4 // number of arguments from the command line
// positions of arguments from the command line
#define KIND_POS 1
#define AMOUNT_POS 2
#define TIME_POS 3

int main(int argc, char* argv[]) {
    Msg msg;
    int k, n, s, partner_pid;
    pid_t pid = getpid();
    int demand_id, resource_id, release_id;
    if (argc < ARGS_NUM)
        fatal("Too few arguments");
    k = atoi(argv[KIND_POS]);
    n = atoi(argv[AMOUNT_POS]);
    s = atoi(argv[TIME_POS]);
    msg.mtype = 1;
    sprintf(msg.mtext, "%d %d %d\n", k, n, pid);
    TRY((demand_id = msgget(DKEY, 0)) == -1, "msget");
    TRY((resource_id = msgget(RSKEY, 0)) == -1, "msget");
    TRY((release_id = msgget(RLKEY, 0)) == -1, "msget");
    TRY(msgsnd(demand_id, &msg, strlen(msg.mtext), 0) == -1, "msgsnd");
    TRY(msgrcv(resource_id, &msg, MAX_DATA_SIZE, pid, 0) == -1, "msgrcv");
    TRY(sscanf(msg.mtext, "%d", &partner_pid) == EOF, "sscanf");
    printf("%d %d %d %d\n", k, n, pid, partner_pid);
    sleep(s);
    msg.mtype = pid;
    // sending empty message
    TRY(msgsnd(release_id, &msg, 0, 0) == -1, "msgsnd");
    printf("%d KONIEC\n", pid);
}
