/* Dawid Lazarczyk, 337614, SO zad. nr 2 */
#include <sys/ipc.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include "utils.h"

#define MAX_KINDS_NUM 99 /* maximum number of kinds of resources */
#define BUFF_SIZE 100 /* size of the buffer used to pass arguments */
#define ARGS_NUM 3 /* number of arguments from the command line */
/* positions of arguments from the command line */
#define KIND_POS 1
#define AMOUNT_POS 2

pthread_mutex_t lock;
/* global shared variables protected by lock */
unsigned int resources[MAX_KINDS_NUM + 1]; /* number of available resources */
unsigned int request[MAX_KINDS_NUM + 1]; /* requested sums of resources */
/* threads waiting for their turn */
pthread_cond_t t_turn[MAX_KINDS_NUM + 1];
/* threads waiting for resources */
pthread_cond_t t_resources[MAX_KINDS_NUM + 1];
/* number of currently running worker threads */
unsigned int running_threads = 0;
int should_terminate = 0;
/* conditional to hang main thread waiting for workers to terminate */
pthread_cond_t termination;
/* attribute for the creation of threads */
pthread_attr_t attr;
/* parameters of the process and keys to the message queues */
int d_id, rs_id, rl_id;

void clean_queues() {
    /* we're deliberately ignoring the errors - we have to delete or at least
     * try to delete all the queues */
    msgctl(d_id, IPC_RMID, NULL);
    msgctl(rs_id, IPC_RMID, NULL);
    msgctl(rl_id, IPC_RMID, NULL);
}

void phandle_error(int err, const char *name) {
    clean_queues();
    psyserr(err, "%s failed", name);
}

void handle_error(const char *name) {
    /* in order not to let the errno variable be overwritten in clean_queues
     * we have to copy its value */
    phandle_error(errno, name);
}

/* this can be used only if the queues were created; we need this to avoid
 * redundation of code; the first macro should be used for pthread functions;
 * the second for other system functions */
#define PTRY(call, e, name) if ((e = (call)) != 0) phandle_error(e, name)
#define TRY(cond, name) if (cond) handle_error(name)

void* worker_thread(void *data) {
    int i, err, k, n[2], pids[2];
    Msg msg;
    TRY(sscanf(data, "%d %d %d %d %d", &k, &n[0], &pids[0], &n[1],
               &pids[1]) == EOF, "sscanf");
    free(data);
    PTRY(pthread_mutex_lock(&lock), err, "mutex_lock");
    while (request[k] > 0) /* we're waiting until we're first of the kind */
        PTRY(pthread_cond_wait(&t_turn[k], &lock), err, "cond_wait");
    request[k] = n[0] + n[1];
    while (request[k] > resources[k])
        PTRY(pthread_cond_wait(&t_resources[k], &lock), err, "cond_wait");
    resources[k] -= request[k];
    request[k] = 0;
    printf("Wątek %lld przydziela %d+%d zasobów %d klientom %d %d, "
           "pozostało %d zasobów\n", (long long)pthread_self(), n[0], n[1],
           k, pids[0], pids[1], resources[k]);
    PTRY(pthread_cond_signal(&t_turn[k]), err, "cond_signal");
    PTRY(pthread_mutex_unlock(&lock), err, "mutex_unlock");
    /* beginning of the critical section */
    /* we're not using a const for '2' in this snippet because it doesn't
     * make sense for other values */
    for (i = 0; i < 2; i++) {
        msg.mtype = pids[i];
        sprintf(msg.mtext, "%d\n", pids[1 - i]);
        TRY(msgsnd(rs_id, &msg, strlen(msg.mtext), 0) == -1, "msgsnd");
    }
    for (i = 0; i < 2; i++)
        TRY(msgrcv(rl_id, &msg, MAX_DATA_SIZE, pids[i], 0) == -1, "msgrcv");
    /* end of the critical section */
    PTRY(pthread_mutex_lock(&lock), err, "mutex_lock");
    resources[k] += n[0] + n[1];
    if (request[k] && request[k] <= resources[k])
        PTRY(pthread_cond_signal(&t_resources[k]), err, "cond_signal");
    running_threads--;
    if (running_threads == 0 && should_terminate)
        PTRY(pthread_cond_signal(&termination), err, "cond_signal");
    PTRY(pthread_mutex_unlock(&lock), err, "mutex_unlock");
    return 0;
}

void* chief_thread(void *arg) {
    int i, err, K = *(int*)arg,
        first_client[MAX_KINDS_NUM + 1], /* pid of the first client of kind */
        amount[MAX_KINDS_NUM + 1]; /* value of request of the first client */
    pthread_t pt;
    Msg msg;
    for (i = 1; i <= K; i++)
        first_client[i] = -1;
    while (1) {
        int k, n, pid;
        TRY(msgrcv(d_id, &msg, MAX_DATA_SIZE, 0, 0) == -1, "msgrcv");
        TRY(sscanf(msg.mtext, "%d %d %d", &k, &n, &pid) == EOF, "sscanf");
        if (first_client[k] > -1) {
             /* we pass arguments through heap to avoid race condition */
            char *buff;
            TRY((buff = malloc(BUFF_SIZE)) == NULL, "malloc");
            sprintf(buff, "%d %d %d %d %d\n", k, amount[k],
                    first_client[k], n, pid);
            first_client[k] = -1;
            PTRY(pthread_mutex_lock(&lock), err, "mutex_lock");
            running_threads++;
            PTRY(pthread_mutex_unlock(&lock), err, "mutex_unlock");
            if ((err = pthread_create(&pt, &attr, worker_thread, buff)) != 0) {
                free(buff);
                phandle_error(err, "pthread_create");
            }
        }
        else {
            first_client[k] = pid;
            amount[k] = n;
        }
    }
}

void init(int K, int N) {
    int i, err;
    key_t flags;
    /* we're creating queues; if keys are already taken, an error will occur */
    flags = S_IRWXU | IPC_CREAT | IPC_EXCL;
    if ((d_id = msgget(DKEY, flags)) == -1) syserr("msget");
    /* in case of the failure we have to clean up */
    if ((rs_id = msgget(RSKEY, flags)) == -1) {
        err = errno;
        msgctl(d_id, IPC_RMID, NULL);
        psyserr(err, "msgget");
    }
    if ((rl_id = msgget(RLKEY, flags)) == -1) {
        err = errno;
        msgctl(d_id, IPC_RMID, NULL);
        msgctl(rs_id, IPC_RMID, NULL);
        psyserr(err, "msget");
    }
    PTRY(pthread_mutex_init(&lock, 0), err, "mutex_init");
    PTRY(pthread_cond_init(&termination, 0), err, "cond_init");
    /* we'll create all the threads as detached to avoid the necessity of
     * joining them */
    PTRY(pthread_attr_init(&attr), err, "attr_init");
    PTRY(pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED), err,
         "setdetachstate");
    for (i = 1; i <= K; i++) {
        resources[i] = N;
        request[i] = 0;
        PTRY(pthread_cond_init(&t_resources[i], 0), err, "cond_init");
        PTRY(pthread_cond_init(&t_turn[i], 0), err, "cond_init");
    }
}

int main(int argc, char* argv[]) {
    int caught, err, i, K, N;
    sigset_t sigs;
    pthread_t chief;
    if (argc < ARGS_NUM)
        fatal("Too few arguments");
    K = atoi(argv[KIND_POS]);
    N = atoi(argv[AMOUNT_POS]);
    sigemptyset(&sigs);
    sigaddset(&sigs, SIGINT);
    /* we're blocking SIGINT for the child threads */
    pthread_sigmask(SIG_BLOCK, &sigs, NULL);
    /* setting up all the resources */
    init(K, N);
    /* creating the chief thread that will receive messages from clients */
    PTRY(pthread_create(&chief, &attr, chief_thread, &K), err,
         "thread_create");
    /* waiting for the SIGINT from the user */
    PTRY(sigwait(&sigs, &caught), err, "sigwait");
    /* cancelling the chief thread; it has enabled deferred cancellation mode
     * by default */
    PTRY(pthread_cancel(chief), err, "thread_cancel");
    PTRY(pthread_mutex_lock(&lock), err, "mutex_lock");
    should_terminate = 1;
    while (running_threads > 0)
        PTRY(pthread_cond_wait(&termination, &lock), err, "cond_wait");
    PTRY(pthread_mutex_unlock(&lock), err, "mutex_unlock");
    PTRY(pthread_mutex_destroy(&lock), err, "mutex_destroy");
    PTRY(pthread_cond_destroy(&termination), err, "cond_destroy");
    PTRY(pthread_attr_destroy(&attr), err, "attr_destroy");
    for (i = 0; i <= K; i++) {
        PTRY(pthread_cond_destroy(&t_resources[i]), err, "cond_destroy");
        PTRY(pthread_cond_destroy(&t_turn[i]), err, "cond_destroy");
    }
    clean_queues();
    exit(0);
}
